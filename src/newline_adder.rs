//! Struct to add a new line to the end of a `Write`r if the last character before
//! a `Drop` wasn't a new line but something was written successfully

use std::io::Write;

/// `Write`r which adds a newline on the end on `Drop` if it is missing and something was written.
/// Stores a reference because that makes tests easier to write
pub struct NewlineAdder<'a, W: Write + Sized> {
    writer: &'a mut W,
    last_byte_written: Option<u8>,
}

impl<'a, W: Write> NewlineAdder<'a, W> {
    /// Constructor
    pub fn new(writer: &'a mut W) -> Self {
        Self {
            writer,
            last_byte_written: None,
        }
    }
}

impl<'a, W: Write> Write for NewlineAdder<'a, W> {
    fn write(&mut self, bytes: &[u8]) -> std::result::Result<usize, std::io::Error> {
        let res = self.writer.write(bytes)?;
        self.last_byte_written = bytes.last().copied();
        Ok(res)
    }

    fn flush(&mut self) -> std::result::Result<(), std::io::Error> {
        self.writer.flush()
    }
}

impl<'a, W: Write> Drop for NewlineAdder<'a, W> {
    fn drop(&mut self) {
        if let Some(last) = self.last_byte_written {
            if last != b'\n' {
                // we can't sensibly handle errors from a drop
                let _ = writeln!(self.writer);
                let _ = self.writer.flush();
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::NewlineAdder;
    use std::io::Write;

    #[test]
    fn add_newline() {
        let mut v = Vec::new();
        {
            let mut proxy = NewlineAdder::new(&mut v);
            write!(&mut proxy, "hi").unwrap();
        }
        assert_eq!(v, vec![b'h', b'i', b'\n']);
    }

    #[test]
    fn dont_add_newline() {
        let mut v = Vec::new();
        {
            let mut proxy = NewlineAdder::new(&mut v);
            writeln!(&mut proxy, "hi").unwrap();
        }
        assert_eq!(v, vec![b'h', b'i', b'\n']);
    }
}
