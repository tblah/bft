//! Process command line arguments
use std::ffi::{OsStr, OsString};
use std::path::Path;

use clap::{App, Arg};

use bft_interp::{CLASSIC_BF_AUTO_GROW, CLASSIC_BF_TAPE_SIZE};
use bft_types::ProgLocation;

/// Global state
pub struct CmdLineArgs {
    /// Path to program to exectute
    pub path: ProgLocation,
    /// Number of cells in BF VM
    pub num_cells: usize,
    /// Auto-grow num_cells with out of bounds accesses
    pub auto_grow: bool,
}

impl Default for CmdLineArgs {
    fn default() -> Self {
        Self {
            path: ProgLocation::Stdin,
            num_cells: CLASSIC_BF_TAPE_SIZE,
            auto_grow: CLASSIC_BF_AUTO_GROW,
        }
    }
}

impl CmdLineArgs {
    /// Construct from command line arguments
    pub fn new() -> Self {
        let default_num_cells_str = format!("{}", CLASSIC_BF_TAPE_SIZE);
        let matches = App::new("BFT")
            .author("Tom Eccles <tom.eccles@codethink.co.uk>")
            .about("Interprets BF programs")
            .version(env!("CARGO_PKG_VERSION"))
            .arg(
                Arg::with_name("path")
                    .value_name("PATH")
                    .help("Input BF file. If this isn't specified, the input is read from stdin")
                    .required(false)
                    .validator_os(path_validator),
            )
            .arg(
                Arg::with_name("num_cells")
                    .help("Number of cells in the BF VM tape")
                    .value_name("N")
                    .default_value(&default_num_cells_str)
                    .validator(tape_size_validator)
                    .takes_value(true)
                    .short("c")
                    .long("cells"),
            )
            .arg(
                Arg::with_name("auto_grow")
                    .help("Allow auto-growing of the BF VM tape")
                    .takes_value(false)
                    .short("e")
                    .long("extensible"),
            )
            .get_matches();
        let path = ProgLocation::new(matches.value_of_os("path"));
        // first unwrap is safe because we provide a default value
        // second unwrap safe because we tested a `parse()` in the argument validation
        let num_cells: usize = matches.value_of("num_cells").unwrap().parse().unwrap();
        let auto_grow: bool = matches.is_present("auto_grow");

        CmdLineArgs {
            path,
            num_cells,
            auto_grow,
        }
    }
}

/// Validate the PATH argument
fn path_validator(arg: &OsStr) -> Result<(), OsString> {
    let path = Path::new(arg);
    if path.exists() && !path.is_dir() {
        Ok(())
    } else {
        Err(OsString::from(format!("{:?} is not a file", arg)))
    }
}

/// Validates the --tape-size N argument
// needless pass by value to conform to `Clap` API
#[allow(clippy::needless_pass_by_value)]
fn tape_size_validator(arg: String) -> Result<(), String> {
    let size = arg
        .parse::<usize>()
        .map_err(|e| format!("Not a usize: {}", e))?;
    if size == 0 {
        Err(String::from("Size cannot be 0"))
    } else {
        Ok(())
    }
}
