//! BF Interpreter
#![warn(missing_docs)]
#![deny(clippy::all)]
#![warn(clippy::pedantic)]

use std::error;
use std::io::{self, Write};

pub use bft_interp::{ClassicBfCell, BFVM};
pub use bft_types::BFProgram;

mod cli;
mod newline_adder;

use cli::CmdLineArgs;
use newline_adder::NewlineAdder;

#[cfg(unix)]
use std::os::unix::io::AsRawFd;

type GenericResult<T> = std::result::Result<T, Box<dyn error::Error>>;

// see termios(3) section on "Canonical and noncanonical mode"
// In canonical mode input is made available line by line instead of after each character
// we want the input after each character so set noncanonical mode
#[cfg(unix)]
fn stdin_set_tty_noncanonical() -> GenericResult<()> {
    if atty::is(atty::Stream::Stdin) {
        let stdin = io::stdin().as_raw_fd();
        let mut term = termios::Termios::from_fd(stdin)?;
        term.c_lflag &= !(termios::ICANON);
        termios::tcsetattr(stdin, termios::TCSANOW, &term)?;
    }
    Ok(())
}
#[cfg(windows)]
fn stdin_set_tty_noncanonical() -> GenericResult<()> {
    Ok(())
}

fn driver() -> GenericResult<()> {
    let args = CmdLineArgs::new();
    run_bft(args)
}

/// Wrapping to run bft
/// ## Errors
/// - I/O error reading program source, program input, or writing program output
/// - Invalid program
pub fn run_bft(args: CmdLineArgs) -> GenericResult<()> {
    let prog = BFProgram::from_source(args.path)?;
    let mut vm: BFVM<ClassicBfCell> = BFVM::new(&prog, args.num_cells, args.auto_grow);

    let mut stdout = io::stdout();
    let mut writer = NewlineAdder::new(&mut stdout);
    stdin_set_tty_noncanonical()?;

    vm.exec(&mut io::stdin(), &mut writer)?;

    Ok(())
}

fn main() {
    if let Err(e) = driver() {
        // print error to stderr
        // if we can't print errors then there's not much we can do. Ignore Result.
        let _ = writeln!(io::stderr(), "{}", e);
        std::process::exit(1);
    }
}
