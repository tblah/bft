//! Datatypes for BF interpreter
//! ```
//! use bft_types::{BFProgram, RawInstruction};
//!
//! let prog = BFProgram::new(None, "1=+.").unwrap();
//!
//! let instrs = prog.instrs();
//! assert_eq!(instrs.len(), 2);
//! ```

#![warn(missing_docs)]
#![deny(clippy::all)]
#![warn(clippy::pedantic)]
#![allow(clippy::enum_glob_use)]

use std::ffi::OsStr;
use std::fmt;
use std::fs;
use std::io;
use std::io::Read;
use std::path::Path;
use std::path::PathBuf;

/// Location to read program from
pub enum ProgLocation {
    /// Standard input
    Stdin,
    /// A path
    Path(PathBuf),
}

impl ProgLocation {
    /// New from clap argument
    #[must_use]
    pub fn new(arg: Option<&OsStr>) -> Self {
        match arg {
            Some(path) => ProgLocation::Path(PathBuf::from(path)),
            None => ProgLocation::Stdin,
        }
    }
}

/// Raw BF instruction
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum RawInstruction {
    /// Increment data pointer
    IncDP,
    /// Decrement data pointer
    DecDP,
    /// Increment the byte at the data pointer
    IncB,
    /// Decrement the byte at the data pointer
    DecB,
    /// Output the byte at the data pointer
    Out,
    /// Accept one byte of input, storing its value in the byte at the data pointer
    In,
    /// If the byte at the data pointer is zero, jump instruction pointer forward to the command after the matching ]
    ///
    /// The `usize` is the index to jump to. `Option`al for parsing implementation. This should never be `None` in a
    /// `BFProgram`.
    JFZ(Option<usize>),
    /// If the byte at the data pointer is nonzero, jump instruction pointer backward to the command after the matching [
    ///
    /// The `usize` is the index to jump to. `Option`al for parsing implementation. This should never be `None` in a
    /// `BFProgram`.
    JBNZ(Option<usize>),
}

impl RawInstruction {
    /// Construt from an ascii character if the character is a valid BF command, otherwise `None`
    /// Not returning `Result` because it is not an error to use characers other than BF commands in a BF program - they are comments
    #[must_use]
    fn from_ascii_char(c: u8) -> Option<Self> {
        use RawInstruction::*;
        match c {
            b'>' => Some(IncDP),
            b'<' => Some(DecDP),
            b'+' => Some(IncB),
            b'-' => Some(DecB),
            b'.' => Some(Out),
            b',' => Some(In),
            b'[' => Some(JFZ(None)),
            b']' => Some(JBNZ(None)),
            _ => None,
        }
    }

    /// Get the character associated with this instruction
    #[must_use]
    pub fn to_char(self) -> char {
        use RawInstruction::*;
        match self {
            IncDP => '>',
            DecDP => '<',
            IncB => '+',
            DecB => '-',
            Out => '.',
            In => ',',
            JFZ(_) => '[',
            JBNZ(_) => ']',
        }
    }
}

impl fmt::Display for RawInstruction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.to_char())
    }
}

/// Instruction with location in input
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct InputInstruction {
    /// The instrution specified
    instr: RawInstruction,
    /// Line number in source file (starting at 1)
    line: usize,
    /// Column in source file (starting at 1)
    col: usize,
}

impl InputInstruction {
    /// Constructor
    #[must_use]
    pub fn new(instr: RawInstruction, line: usize, col: usize) -> Self {
        Self { instr, line, col }
    }

    /// Try to construct from ascii byte `c`. If `c` isn't a BF instruction return `None`.
    ///
    /// Branch addresses are set to `None` for [ and ]
    #[must_use]
    fn try_new(c: u8, line: usize, col: usize) -> Option<Self> {
        let instr = RawInstruction::from_ascii_char(c)?;
        Some(Self::new(instr, line, col))
    }

    /// Get the `RawInstruction`
    #[must_use]
    pub fn instr(&self) -> RawInstruction {
        self.instr
    }

    /// Replace the `RawInstruction`
    #[must_use]
    pub fn instr_mut(&mut self) -> &mut RawInstruction {
        &mut self.instr
    }

    /// Line number in the original file
    #[must_use]
    pub fn line(&self) -> usize {
        self.line
    }

    /// Column in the original file
    #[must_use]
    pub fn col(&self) -> usize {
        self.col
    }
}

#[derive(Debug)]
/// Errors while parsing a program
pub enum Error {
    /// Error reading from the source file
    IO(io::Error, Option<PathBuf>),
    /// Unbalanced brackets in the input file at this `InputInstruction`
    UnbalancedBrackets(InputInstruction, Option<PathBuf>),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut write_path = |p: &Option<PathBuf>| {
            if let Some(s) = p {
                write!(f, "{}:", s.to_string_lossy())
            } else {
                Ok(())
            }
        };

        match self {
            Self::IO(e, p) => {
                write_path(p)?;
                write!(f, "{}", e)
            }
            Self::UnbalancedBrackets(i, p) => {
                write_path(p)?;
                write!(f, "{}:{} Unbalanced {}", i.line(), i.col(), i.instr())
            }
        }
    }
}

impl std::error::Error for Error {}

impl Error {
    /// Construct a new IO error
    #[must_use]
    pub fn new_io(path: Option<PathBuf>, e: io::Error) -> Self {
        Self::IO(e, path)
    }

    /// Construct a new unmatched bracket error
    #[must_use]
    pub fn new_unmatched_bracket(path: Option<PathBuf>, i: InputInstruction) -> Self {
        Self::UnbalancedBrackets(i, path)
    }
}

/// Representation of a BF program
#[derive(Debug)]
pub struct BFProgram {
    /// Filename the program was loaded from. Stored as a reference because this is convenient for borrow checking in bft_interp
    path: Option<PathBuf>,
    /// Parsed instructions
    instrs: Vec<InputInstruction>,
}

impl BFProgram {
    /// Parse a file into a `BFProgram`.
    /// TODO: this needlessly forces unicode validity
    /// ## Errors
    /// - I/O from reading the file
    /// - Source file contained unmatched [ or ]
    pub fn from_file(path: &impl AsRef<Path>) -> Result<Self, Error> {
        Self::from_source(ProgLocation::Path(path.as_ref().to_owned()))
    }

    /// Parse from a `ProgramLocation`
    ///
    /// TODO: needlessly forces unicode validity
    ///
    /// Note: this assumes stdin can be read to the end - this is not a REPL
    /// ## Errors
    /// - I/O from reading file or stdin
    /// - Source file contained unmatched [ or ]
    pub fn from_source(source: ProgLocation) -> Result<Self, Error> {
        let mut contents = String::new();
        let mut optional_path = None;

        match source {
            ProgLocation::Path(p) => {
                let mut f = fs::File::open(&p).map_err(|e| Error::new_io(optional_path, e))?;
                optional_path = Some(p);
                f.read_to_string(&mut contents)
            }
            ProgLocation::Stdin => {
                let mut stdin = io::stdin();
                stdin.read_to_string(&mut contents)
            }
        }
        .map_err(|e| Error::new_io(optional_path.clone(), e))?;

        Self::new(optional_path, &contents)
    }

    /// Constructor for files which are already read in or fake files.
    ///
    /// TODO: `&str` needlessly forces unicode validity
    ///
    /// The `path` argument is just the path reported in error messages. To read in a file use `BFProgram::from_file`
    /// ```
    ///# use bft_types::BFProgram;
    /// let prog = BFProgram::new(None, "1=+.");
    /// ```
    /// ## Errors
    /// - Unmatched [ or ]
    pub fn new(path: Option<PathBuf>, contents: &str) -> Result<Self, Error> {
        let mut instrs: Vec<InputInstruction> = Vec::new();
        let mut open_bracket_stack = Vec::new();

        for (line_no, line) in contents.lines().enumerate() {
            for (col, c) in line.bytes().enumerate() {
                // increment counters to go from starting at 0 to starting at 1
                if let Some(mut input_instr) = InputInstruction::try_new(c, line_no + 1, col + 1) {
                    // the index this instruction will be inserted at
                    let this_index = instrs.len();

                    // Add jump addresses to [ and ]
                    match input_instr.instr() {
                        RawInstruction::JFZ(_) => {
                            // We don't know where the ] is yet so store the index of this instrution
                            // so we can find it later to add the jump address
                            open_bracket_stack.push(this_index);
                        }
                        RawInstruction::JBNZ(_) => {
                            // get the matching [
                            let start_index = if let Some(i) = open_bracket_stack.pop() {
                                i
                            } else {
                                return Err(Error::new_unmatched_bracket(path, input_instr));
                            };
                            let start = instrs.get_mut(start_index).expect("vec never shrunk");

                            // fill in the jump address for [
                            // +1 to go to the instruction after this one
                            *start.instr_mut() = RawInstruction::JFZ(Some(this_index + 1));

                            // fill in jump address for ]
                            let this_instr = RawInstruction::JBNZ(Some(start_index));
                            *input_instr.instr_mut() = this_instr;
                        }
                        _ => {}
                    };

                    instrs.push(input_instr)
                } // else { skip comment char }
            }
        }

        if !open_bracket_stack.is_empty() {
            // unmatched [
            return Err(Error::new_unmatched_bracket(
                path,
                instrs[open_bracket_stack[0]].clone(),
            ));
        }

        Ok(Self { path, instrs })
    }

    /// Get the filename this program was loaded from
    #[must_use]
    pub fn path(&self) -> Option<&Path> {
        self.path.as_deref()
    }

    /// Get the instructions constituting this program
    #[must_use]
    pub fn instrs(&self) -> &[InputInstruction] {
        &self.instrs
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn brackets_test(prog_contents: &str, expected: &Result<(), InputInstruction>) {
        let res = BFProgram::new(None, prog_contents).map(|_| ());
        // mess around because super::Error doesn't implement PartialEq (because io::Error doesn't)
        if expected.is_ok() {
            assert!(res.is_ok())
        } else if let Error::UnbalancedBrackets(i, _) = res.unwrap_err() {
            assert_eq!(&i, expected.as_ref().unwrap_err())
        } else {
            panic!("Error is not an UnbalancedBrackets")
        }
    }

    #[test]
    fn brackets_empty() {
        brackets_test("", &Ok(()));
    }

    #[test]
    fn brackets_balanced() {
        brackets_test("[]", &Ok(()));
    }

    #[test]
    fn brackets_unbalanced_open() {
        let instr = InputInstruction::new(RawInstruction::JFZ(None), 1, 3);
        brackets_test("[][", &Err(instr));
    }

    #[test]
    fn brackets_unbalanced_close() {
        let instr = InputInstruction::new(RawInstruction::JBNZ(None), 1, 1);
        brackets_test("][]", &Err(instr));
    }

    #[test]
    fn brackets_other_chars() {
        brackets_test("+-<>[.,]", &Ok(()))
    }

    #[test]
    fn nested_brackets() {
        let s = "+[-[,]].";
        let prog = BFProgram::new(None, s).unwrap();

        let raw_instrs = vec![
            RawInstruction::IncB,          // +
            RawInstruction::JFZ(Some(7)),  // [
            RawInstruction::DecB,          // -
            RawInstruction::JFZ(Some(6)),  // [
            RawInstruction::In,            // ,
            RawInstruction::JBNZ(Some(3)), // ]
            RawInstruction::JBNZ(Some(1)), // ]
            RawInstruction::Out,           // .
        ];

        // transform RawInstructions into InputInstructions
        let expected: Vec<InputInstruction> = raw_instrs
            .iter()
            .enumerate()
            .map(|(index, instr)| InputInstruction::new(*instr, 1, index + 1))
            .collect();

        assert_eq!(prog.instrs().to_vec(), expected);
    }
}
