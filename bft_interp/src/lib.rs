//! BF interpreter
//! ## Example
//! ```
//! use std::io;
//! use bft_interp::BFVM;
//! use bft_types::BFProgram;
//!
//! let prog = BFProgram::new(None, "1=+.").unwrap();
//! let mut vm: BFVM<u8> = BFVM::new(&prog, 0, false);
//!
//! vm.exec(&mut io::stdin(), &mut io::stdout());
//! ```

#![warn(missing_docs)]
#![deny(clippy::all)]
#![warn(clippy::pedantic)]
#![allow(clippy::enum_glob_use)]

use std::convert::TryFrom;
use std::fmt;
use std::io;

use bft_types::{BFProgram, RawInstruction};

mod err;
pub use err::{Error, RawError};

/// Default value of the tape starting length for `BFVM`
pub const CLASSIC_BF_TAPE_SIZE: usize = 30_000;
/// Default value of tape auto-grow for `BFVM`
pub const CLASSIC_BF_AUTO_GROW: bool = false;
/// Default cell type for classic BF
pub type ClassicBfCell = u8;

/// Describes appropiate types for cells in the BF VM
pub trait Cell: fmt::Display + Default + Sized {
    /// Increment cell. Wrapping addition.
    fn inc(&mut self);

    /// Decrement cell. Wrapping subtraction.
    fn dec(&mut self);

    /// Parse from input. Report errors as `String`s to match `Error::Input`
    /// ## Errors
    /// - `io::Error` from `io::Read::read`
    /// - Returns `Ok(None)` when encountering EOF
    fn from_input(r: &mut impl io::Read) -> Result<Option<Self>, String>;

    /// Output cell
    /// ## Errors
    /// - `io::Error` from `io::Write::write`
    /// - Cell is not valid ascii
    fn output(&self, w: &mut impl io::Write) -> Result<(), String>;

    /// Compare with 0
    fn is_zero(&self) -> bool;
}

/// Implementation of `Cell` for numeric types
macro_rules! impl_cell {
    ($($t: ty),+) => {$(
        impl Cell for $t {
            fn inc(&mut self) {
                *self = self.wrapping_add(1)
            }

            fn dec(&mut self) {
                *self = self.wrapping_sub(1)
            }

            /// Read one byte (i.e. ascii) from input and set cell to that
            fn from_input(r: &mut impl io::Read) -> Result<Option<$t>, String> {
                let mut buf = [0; 1];
                if let Err(e) =  r.read_exact(&mut buf) {
                    if e.kind() == io::ErrorKind::UnexpectedEof {
                        // input wasn't a tty and we ran out of it, pass this up
                        return Ok(None);
                    }
                    return Err(e.to_string());
                }
                Ok(Some(buf[0].into()))
            }

            /// output Cell as ascii
            fn output(&self, w: &mut impl io::Write) -> Result<(), String> {
                let conv_err_str = || format!("Cannot output {} because it is not ascii", self);
                let b = u8::try_from(*self).map_err(|_| conv_err_str())?;
                if b.is_ascii() {
                    let buf = [b];
                    w.write(&buf).map_err(|e| e.to_string())?;
                    w.flush().map_err(|e| e.to_string())?;
                    Ok(())
                } else {
                    Err(conv_err_str())
                }
            }

            fn is_zero(&self) -> bool {
                *self == 0
            }
        }
    )+};
}
// don't support `i8` because it can't be created from a `u8` safely
impl_cell!(u8, u16, u32, u64, u128, usize, i16, i32, i64, i128, isize);

/// BF Virtual Machine
#[derive(Debug)]
pub struct BFVM<'a, C> {
    head_location: usize,
    program_counter: usize,
    cells: Vec<C>,
    // making this a borrow means we can hold a borrow of prog
    // and a mutable borrow of self at the same time
    prog: &'a BFProgram,
    auto_grow: bool,
}

impl<'a, C: Cell> BFVM<'a, C> {
    /// Constructor
    /// `num_cells` is the initial number of cells on the tape. If
    /// 0 is given then `CLASSIC_BF_TAPE_SIZE` is used.
    /// `auto_grow` controls whether the tape should automatically grow
    /// ## Example
    /// ```
    ///# use bft_interp::BFVM;
    /// use bft_types::BFProgram;
    /// let prog = BFProgram::new(None, "a bf program here").unwrap();
    /// let vm: BFVM<i128> = BFVM::new(&prog, 42, true);
    /// ```
    pub fn new(prog: &'a BFProgram, mut num_cells: usize, auto_grow: bool) -> Self {
        if num_cells == 0 {
            num_cells = CLASSIC_BF_TAPE_SIZE;
        }
        let mut cells = Vec::with_capacity(num_cells);
        cells.resize_with(num_cells, Default::default);

        Self {
            prog,
            cells,
            auto_grow,
            head_location: 0,
            program_counter: 0,
        }
    }

    /// Run a `BFProgram`
    /// ## Example
    /// ```
    ///# use bft_interp::BFVM;
    /// use std::default::Default;
    /// use bft_types::BFProgram;
    /// use std::io;
    ///
    /// let prog = BFProgram::new(None, "1=+.").unwrap();
    /// let mut vm: BFVM<u8> = BFVM::new(&prog, 42, false);
    ///
    /// vm.exec(&mut io::stdin(), &mut io::stdout()).unwrap();
    /// ```
    /// ## Errors
    /// - Anything in `Error`
    /// ## Panics
    /// If `self.prog` is malformed, e.g. a [ without a matching index.
    /// It shouldn't be possible to construct a malformed `BFProgram`.
    pub fn exec(
        &mut self,
        input: &mut impl io::Read,
        output: &mut impl io::Write,
    ) -> Result<(), Error> {
        use RawInstruction::*;

        let instrs = self.prog.instrs();

        // Interpret invalid program_counter as the end of the program
        while let Some(input_instr) = instrs.get(self.program_counter) {
            let mut jumped = false;

            match input_instr.instr() {
                IncDP => self.inc_tape_ptr(),
                DecDP => self.dec_tape_ptr(),
                IncB => self.inc_cell(),
                DecB => self.dec_cell(),
                Out => self.output_cell(output),
                In => match self.input_cell(input) {
                    Ok(()) => Ok(()),
                    Err(e) => {
                        // interpret end of input as the end of the program loop
                        if e == RawError::OutOfInput {
                            return Ok(());
                        } else {
                            Err(e)
                        }
                    }
                },
                JFZ(jmp_index) => self.jump_if_zero(jmp_index).map(|b| jumped = b),
                // jump unconditionally to the JFZ and let that instr check the condition
                JBNZ(jmp_index) => self.jump(jmp_index).map(|b| jumped = b),
            }
            .map_err(|e| e.upgrade(self.prog.path(), input_instr.clone()))?;

            if !jumped {
                self.program_counter += 1;
            }
        }

        Ok(())
    }

    /// Increment tape pointer
    fn inc_tape_ptr(&mut self) -> Result<(), RawError> {
        let new_ptr = self.head_location + 1;
        if new_ptr == self.cells.len() {
            if self.auto_grow {
                self.cells.resize_with(new_ptr + 1, Default::default);
            } else {
                return Err(RawError::BadTapePointer);
            }
        }
        self.head_location = new_ptr;
        Ok(())
    }

    /// Decrement tape pointer
    fn dec_tape_ptr(&mut self) -> Result<(), RawError> {
        if let Some(new_ptr) = self.head_location.checked_sub(1) {
            self.head_location = new_ptr;
            Ok(())
        } else {
            Err(RawError::BadTapePointer)
        }
    }

    /// Get mutable reference to cell at head pointer
    fn get_cell_mut(&mut self) -> &mut C {
        self.cells
            .get_mut(self.head_location)
            .expect("BUG: head_location should always be valid")
    }

    /// Increment cell pointed to by `self.head_location`
    fn inc_cell(&mut self) -> Result<(), RawError> {
        self.get_cell_mut().inc();
        Ok(())
    }

    /// Decrement cell pointed to by `self.head_location`
    fn dec_cell(&mut self) -> Result<(), RawError> {
        self.get_cell_mut().dec();
        Ok(())
    }

    /// Output cell pointed to by `self.head_location`
    fn output_cell(&self, out: &mut impl io::Write) -> Result<(), RawError> {
        let cell = &self.cells[self.head_location];
        cell.output(out).map_err(RawError::Output)
    }

    /// Input into cell pointed to by `self.head_location`
    fn input_cell(&mut self, input: &mut impl io::Read) -> Result<(), RawError> {
        let cell = self.get_cell_mut();
        *cell = match C::from_input(input).map_err(RawError::Input)? {
            Some(c) => c,
            None => return Err(RawError::OutOfInput),
        };
        Ok(())
    }

    /// Unconditional jump for JBNZ, returns Ok(true) on success
    fn jump(&mut self, pc: Option<usize>) -> Result<bool, RawError> {
        self.program_counter = pc.expect("Bug in BFProgram");
        Ok(true)
    }

    /// JFZ (jump forward on zero) instruction [, return true if we branched
    fn jump_if_zero(&mut self, pc: Option<usize>) -> Result<bool, RawError> {
        let cell = &self.cells[self.head_location];
        if cell.is_zero() {
            self.jump(pc)?;
            Ok(true)
        } else {
            Ok(false)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{BFProgram, BFVM};

    #[test]
    fn inc_dec_tape_ptr() {
        let prg = BFProgram::new(None, "dummy program").unwrap();
        let mut vm = BFVM::<u8>::new(&prg, 42, false);
        assert_eq!(vm.head_location, 0);
        let res = vm.inc_tape_ptr();
        assert_eq!(res, Ok(()));
        assert_eq!(vm.head_location, 1);
        let res = vm.dec_tape_ptr();
        assert_eq!(res, Ok(()));
        assert_eq!(vm.head_location, 0)
    }

    #[test]
    fn dec_tape_ptr_below_zero() {
        let prg = BFProgram::new(None, "dummy program").unwrap();
        let mut vm = BFVM::<u8>::new(&prg, 42, false);
        assert_eq!(vm.head_location, 0);
        let res = vm.dec_tape_ptr();
        assert!(res.is_err());
        assert_eq!(vm.head_location, 0);
    }

    #[test]
    fn inc_tape_ptr_too_high() {
        let prg = BFProgram::new(None, "dummy program").unwrap();
        let mut vm = BFVM::<u8>::new(&prg, 2, false);
        let res = vm.inc_tape_ptr();
        assert_eq!(res, Ok(()));
        assert_eq!(vm.head_location, 1);
        let res = vm.inc_tape_ptr();
        assert!(res.is_err());
        assert_eq!(vm.head_location, 1);
    }

    #[test]
    fn inc_tape_ptr_grow() {
        let prg = BFProgram::new(None, "dummy program").unwrap();
        let mut vm = BFVM::<u8>::new(&prg, 1, true);
        assert_eq!(vm.head_location, 0);
        assert_eq!(vm.cells.len(), 1);

        let res = vm.inc_tape_ptr();
        assert_eq!(res, Ok(()));
        assert_eq!(vm.head_location, 1);
        assert_eq!(vm.cells.len(), 2);
        assert_eq!(vm.cells[1], 0);
    }

    #[test]
    fn output_byte() {
        let prg = BFProgram::new(None, "dummy program").unwrap();
        let mut vm = BFVM::<u8>::new(&prg, 42, false);

        let byte = b'A';
        *vm.get_cell_mut() = byte;

        let mut output = Vec::new();
        let res = vm.output_cell(&mut output);
        assert!(res.is_ok());
        assert_eq!(&output, &[byte]);
    }

    #[test]
    fn input_byte() {
        let prg = BFProgram::new(None, "dummy program").unwrap();
        let mut vm = BFVM::<u8>::new(&prg, 42, false);
        let byte = b'A';

        let mut input: &[u8] = &[byte];
        let res = vm.input_cell(&mut input);
        assert!(res.is_ok());
        assert_eq!(*vm.cells.get(0).unwrap(), byte);
    }
}
