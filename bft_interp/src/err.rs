//! Errors for the interpreter

use std::error;
use std::fmt;
use std::path::Path;
use std::path::PathBuf;

use bft_types::InputInstruction;

/// Runtime errors from the interpreter
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum RawError {
    /// Unbalanced square bracket
    UnbalancedBracket,
    /// Program is unreasonably long (counter overflow)
    ProgramTooLong,
    /// Tape pointer fell off the the beginning or end of the tape
    BadTapePointer,
    /// Error writing to output. Describing error with `String` because
    /// `io::Error` doesn't implement `PartialEq`
    Output(String),
    /// Error reading from input. Describing error with `String` because
    /// `io::Error` doesn't implement `PartialEq`
    Input(String),
    /// Encountered EOF when reading input
    OutOfInput,
}

impl RawError {
    /// Upgrade to a fully ellaborated `Error`
    #[must_use]
    pub fn upgrade(self, path: Option<&Path>, i: InputInstruction) -> Error {
        Error {
            source: path.map(Path::to_path_buf),
            instr: i,
            err: self,
        }
    }
}

/// Elaborated error type able to describe where the error originated
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Error {
    /// The path to the source file, if available
    source: Option<PathBuf>,
    /// The offending instruction
    instr: InputInstruction,
    /// The actual error
    err: RawError,
}

impl Error {
    /// Construct new elaborated `Error`
    #[must_use]
    pub fn new(err: RawError, source: Option<PathBuf>, instr: InputInstruction) -> Self {
        Self { err, source, instr }
    }

    /// Get the actual error
    #[must_use]
    pub fn err(&self) -> RawError {
        self.err.clone()
    }

    /// Get the `InputInstruction` at which the error occured
    #[must_use]
    pub fn instr(&self) -> InputInstruction {
        self.instr.clone()
    }

    /// Get the path to the source file at which this error occured
    #[must_use]
    pub fn source(&self) -> Option<PathBuf> {
        self.source.clone()
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut print_err = |msg: &str| -> fmt::Result {
            if let Some(path) = &self.source {
                write!(f, "{}:", path.to_string_lossy())?;
            }

            write!(f, "{}:{}: ", self.instr.line(), self.instr.col())?;

            write!(f, "{}", msg)
        };

        match &self.err {
            RawError::UnbalancedBracket => print_err("Unbalenced [ or ]"),
            RawError::BadTapePointer => print_err("Tape pointer out of tape"),
            RawError::Output(err) | RawError::Input(err) => print_err(&err),
            RawError::ProgramTooLong => write!(f, "Program too long"),
            RawError::OutOfInput => write!(f, "Hit EOF while reading input"),
        }
    }
}

impl error::Error for Error {}
