use std::io;

use bft_interp::BFVM;
use bft_types::BFProgram;

/// Test we can run `hello_world.bf`
#[test]
fn hello_world() {
    let prog = BFProgram::from_file(&"tests/hello_world.bf").unwrap();
    let mut vm = BFVM::<u8>::new(&prog, 0, false);
    let mut output = Vec::new();
    vm.exec(&mut io::empty(), &mut output).unwrap();

    let expected: Vec<u8> = "hello world".bytes().collect();
    assert_eq!(expected, output);
}
