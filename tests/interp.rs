use std::fs::File;
use std::io::Cursor;
use std::io::Read;

use bft_interp::BFVM;
use bft_types::BFProgram;

#[test]
fn intrp_hello_world() {
    let mut hello_world_src = String::new();
    File::open("tests/hello_world.bf")
        .unwrap()
        .read_to_string(&mut hello_world_src)
        .unwrap();
    // interpreter assumes the input program ends with a !
    hello_world_src += "!";
    let expected: Vec<u8> = "hello world".bytes().collect();

    let prog = BFProgram::from_file(&"tests/interp.bf").unwrap();
    let mut vm = BFVM::<u8>::new(&prog, 1, true);

    let mut input: Cursor<Vec<u8>> = Cursor::new(hello_world_src.bytes().collect());
    let mut output = Vec::new();
    vm.exec(&mut input, &mut output).unwrap();

    println!("output: {}", String::from_utf8_lossy(&output));

    assert_eq!(output, expected);
}
