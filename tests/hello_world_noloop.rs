use std::io;

use bft_interp::BFVM;
use bft_types::BFProgram;

/// Generates a bf program to print a string, assuming DP points to 0
/// assumes s contains only ascii
fn bf_print_str(s: &str) -> String {
    let mut bf_prog = String::new();
    for c in s.bytes() {
        // get character into cell by repeated addition
        for _ in 0..c {
            bf_prog.push('+');
        }

        // output character
        bf_prog.push('.');

        // reset cell to 0
        for _ in 0..c {
            bf_prog.push('-');
        }
    }
    bf_prog
}

#[test]
fn hello_world_noloop() {
    let s = "Hello world!";
    let prog_str = bf_print_str(s);
    let prog = BFProgram::new(None, &prog_str).unwrap();

    let mut input = io::empty();
    let mut output = Vec::new();

    let mut vm = BFVM::<u8>::new(&prog, 0, false);
    let res = vm.exec(&mut input, &mut output);

    assert!(res.is_ok());
    assert_eq!(&output, &s.bytes().collect::<Vec<u8>>());
}
