use bft_interp::BFVM;
use bft_types::BFProgram;

use std::io;

#[test]
fn hello_world_complex() {
    // from https://esolangs.org/wiki/Brainfuck
    // "This is a slightly more complex variant that often triggers interpreter bugs. This uses cell values below zero"
    let src = ">++++++++[-<+++++++++>]<.>>+>-[+]++>++>+++[>[->+++<<+++>]<<]>-----.>->
               +++..+++.>-.<<+[>[+>+]>>]<--------------.>>.+++.------.--------.>+.>+.";
    let expected: Vec<u8> = "Hello World!\n".bytes().collect();
    let mut output = Vec::new();

    let prog = BFProgram::new(None, src).unwrap();
    // also test autogrow while we're at it
    let mut unsigned_vm: BFVM<u8> = BFVM::new(&prog, 1, true);
    let mut signed_vm: BFVM<i16> = BFVM::new(&prog, 1, true);

    unsigned_vm.exec(&mut io::empty(), &mut output).unwrap();
    assert_eq!(output, expected);

    output.clear();
    signed_vm.exec(&mut io::empty(), &mut output).unwrap();
    assert_eq!(output, expected);
}
